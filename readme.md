# Message exchange web app with socket.io and node.js

## About
Created node.js web application using Socket.io and ExpressJS. Features include: 

- Configurable list of public/private channels
- Private channels are protected by secret passphrase
- Public channel sends a greeting message upon joining
- Public channel sends a random number to members every 5 seconds

## Installation
- Clone the repository
- Run `npm install` to install dependencies
- Start server by running `npm start`
- Web application will be available on http://localhost:3000

## Stack
- node.js - runtime
- Express.js - web app server
- Handlebars.js - template engine
- ES6 - modern JavaScript
- Socket.io - realtime, bi-directional communication engine