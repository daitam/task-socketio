const socket = io();

$('form').submit(() => {
    let msg = $('#m').val();
    if (msg) {
      socket.emit('message', {msg, room});
      $('#m').val('');
    }
    return false;
});

socket.on('connect', () => {
  attemptJoin();
});

socket.on('message', (msg) => {
    $('#messages').append($('<li>').text(msg));
    scrollDown();
});

socket.on('random number message', (msg) => {
  $('#messages').append($('<li class="text-muted">').append($('<small>').text(msg)));
  scrollDown();
});

socket.on('denied', (msg) => {
  attemptJoin(msg, true);
});

const attemptJoin = (msg, isProtected) => {
  let data = { room };
  if (isProtected) {
    let secret = prompt(msg);
    if(!secret) {
      return;
    }
    data.secret = secret;
  }

  socket.emit('join', data);
}

// Scroll down to latest message
const scrollDown = () => {
  $('#messages').animate({scrollTop: $('#messages').height()});
}