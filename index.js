const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const exphbs  = require('express-handlebars');
const port = 3000;

app.use(express.static('public'));
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// List of rooms
let rooms = [
  {
    code: 'public-1',
    title: 'Public Room #1',
    count: 0,
    initialized : false,
    private: false,
    secret: null,
  },
  {
    code: 'public-2',
    title: 'Public Room #2',
    count: 0,
    initialized : false,
    private: false,
    secret: null,
  },
  {
    code: 'private-1',
    title: 'Private Room #1',
    count: 0,
    initialized : false,
    private: true,
    secret: 'secret',
  },
  {
    code: 'private-2',
    title: 'Private Room #2',
    count: 0,
    initialized : false,
    private: true,
    secret: 'secret',
  },
];

server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// Route: list of rooms
app.get('/', (req, res) => {
  res.render('home', { rooms });
});

// Route: list of rooms (redirects)
app.get('/rooms', (req, res) => {
  res.redirect('/');
})

// Route: room
app.get('/rooms/:roomCode', (req, res) => {
  const room = getRoom(req.params.roomCode);
  
  if (!room) {
    res.redirect('/');
    return;
  }

  res.render('room', { room: room, pageTitle: room.title });
});

io.on('connection', (socket) => {
  // Handle room join attempt
  socket.on('join', (data) => {
    let room = getRoom(data.room);
    
    if (!room) {
      socket.emit('message', 'Channel no longer exists');
      return;
    }

    if (room.private && !socket.roomCode) {
      // Protected room
      if (data.secret !== room.secret) {
        denyRoom(socket, room);
        return;
      }
    }
    
    // Add to room and increment room member count
    socket.join(data.room);
    room.count++;
    socket.roomCode = data.room;
    
    // Greet only on public rooms
    if (!room.private) {
      socket.emit('message', `Welcome to ${room.title}`);
    }

    // Start to periodically emit random numbers to newly initialized rooms
    if (!room.initialized && !room.private) {
      setInterval(() => {
        let rnd = Math.floor((Math.random() * 100) + 1);
        io.in(data.room).emit('random number message', `Random number ${rnd}`);
      }, 5000);
      
      room.initialized = true;
    };

    console.log(`New user joined ${room.title} room`);
  });
  
  
  // Handle "count" command or emit message to room
  socket.on('message', (data) => {
    let room = getRoom(data.room);

    if(!socket.roomCode) {
      // Case when user has canceled secret prompt and is not authorized to send messages.
      denyRoom(socket, room);
      return;
    }
    
    if (data.msg === 'count') {
      socket.emit('message', `Room ${room.title} has ${room.count} user${room.count != 1 ? 's' : ''}`);
    } else {
      io.in(data.room).emit('message', data.msg);
    }
  });
  
  // Decrement user count on room disconnect
  socket.on('disconnect', () => {
    let room = getRoom(socket.roomCode);
    if (room) {
      room.count = room.count > 1 ? room.count - 1 : 0;
      
      console.log(`User left ${room.title} room`);
    }
  });
});

// Retrieve room from rooms by code
const getRoom = (code) => { return rooms.find(x => x.code === code) };

// Handle 
const denyRoom = (socket, room) => {
  socket.emit('denied', `Please provide secret to enter ${room.title} room`);
};